package CIMB

import assingnment.ClimbingStairs
import assingnment.StockPriceProfit
import assingnment.VersionComparison
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


@SpringBootApplication
class AssignmentCimbApplication

fun main(args: Array<String>) {
	runApplication<AssignmentCimbApplication>(*args)
	val prices1 = intArrayOf(7, 1, 5, 3, 6, 4)
	val prices2 = intArrayOf(7,6, 4, 3, 1)
	val prices3 = intArrayOf(5,3, 7, 2, 9,10)
	val stockPriceProfit = StockPriceProfit()

	val maxProfit1 = stockPriceProfit.maxProfit(prices1)
	val maxProfit2 = stockPriceProfit.maxProfit(prices2)
	val maxProfit3 = stockPriceProfit.maxProfit(prices3)

	println("*******************************************************")
	println("Problem 1 : ")
	println("Stock price: ${prices1.joinToString()} Max profit for prices: $maxProfit1")
	println("Stock price: ${prices2.joinToString()} Max profit for prices: $maxProfit2")
	println("Stock price: ${prices3.joinToString()} Max profit for prices: $maxProfit3")


	val n1 = 2
	val n2 = 3
	val n3 = 45

	val climbingStairs = ClimbingStairs()
	val climbStairs1 = climbingStairs.climbStairs(n1)
	val climbStairs2 = climbingStairs.climbStairs(n2)
	val climbStairs3 = climbingStairs.climbStairs(n3)

	println("*******************************************************")
	println("Problem 3 : ")
	println("Ways to climb $n1 stairs: $climbStairs1 ")
	println("Ways to climb $n2 stairs: $climbStairs2")
	println("Ways to climb $n3 stairs: $climbStairs3")


	val v1 = "1.01"
	val v2 = "1.001"
	val v3 = "1.0"
	val v4 = "1.0.0"
	val v5 = "0.1"
	val v6 = "1.1"

	val versionComparison = VersionComparison()
	val compareVersions1 = versionComparison.compareVersions(v1, v2)
	val compareVersions2 = versionComparison.compareVersions(v3, v4)
	val compareVersions3 = versionComparison.compareVersions(v5, v6)

	println("*******************************************************")
	println("Problem 2 : ")
	println("Comparison of $v1 and $v2: $compareVersions1")
	println("Comparison of $v3 and $v4: $compareVersions2")
	println("Comparison of $v5 and $v6: $compareVersions3")

}


