package assingnment

class ClimbingStairs {
//    ## Q3
//    You are climbing a staircase. It takes `n` steps to reach the top.
//    Each time you can either climb `1` or `2` steps. In how many distinct ways can you climb to the top?
//
//    ### Example 1:
//    **Input**: n = 2 &lt;/br&gt;
//    **Output**: 2 &lt;/br&gt;
//    **Explanation**: There are two ways to climb to the top.
//    1. 1 step + 1 step
//    2. 2 steps
//
//    ### Example 2:
//    **Input**: n = 3 &lt;/br&gt;
//    **Output**: 3 &lt;/br&gt;
//    **Explanation**: There are three ways to climb to the top.
//    1. 1 step + 1 step + 1 step
//    2. 1 step + 2 steps
//    3. 2 steps + 1 step
//
//    ### Constraints:
//    - `1 &lt;= n &lt;= 45`

    fun climbStairs(n: Int): Int{
        if(n <= 2) return n
        val dp = IntArray(n + 1)
        dp[0] =1
        dp[1] = 1
        for (i in 2..n){
            dp[i] = dp[i-1] + dp[i-2]
        }
        return dp[n]
    }
}