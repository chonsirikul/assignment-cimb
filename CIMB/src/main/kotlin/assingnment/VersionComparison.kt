package assingnment

class VersionComparison {

//    ## Q2
//    Given two version numbers, `version1` and `version2`, compare them.
//    Version numbers consist of **one or more revisions** joined by a dot `&#39;.&#39;`. Each revision consists of **digits** and may contain leading **zeros**. Every revision
//    contains **at least one character**. Revisions are **0-indexed from left to right**, with the leftmost revision being revision 0, the next revision being revision 1,
//    and so on. For example `2.5.33` and `0.1` are valid version numbers.
//
//    To compare version numbers, compare their revisions in **left-to-right order**. Revisions are compared using their **integer value ignoring any leading zeros**.
//    This means that revisions 1 and 001 are considered **equal**. If a version number does not specify a revision at an index, then **treat the revision as** `0`. For
//    example, version `1.0` is less than version `1.1` because their revision 0s are the same, but their revision 1s are `0` and `1` respectively, and `0 &lt; 1`.
//    Return the following:
//    - If `version1 &lt; version2`, return `-1`.
//    - If `version1 &gt; version2`, return `1`.
//    - Otherwise, return `0`.
//
//    ### Example 1:
//    **Input**: version1 = &quot;1.01&quot;, version2 = &quot;1.001&quot; &lt;/br&gt;
//    **Output**: 0 &lt;/br&gt;
//    **Explanation**: Ignoring leading zeroes, both &quot;01&quot; and &quot;001&quot; represent the same integer &quot;1&quot;.
//    ### Example 2:
//    **Input**: version1 = &quot;1.0&quot;, version2 = &quot;1.0.0&quot; &lt;/br&gt;
//    **Output**: 0 &lt;/br&gt;
//    **Explanation**: version1 does not specify revision 2, which means it is treated as &quot;0&quot;.
//    ### Example 3:
//    **Input**: version1 = &quot;0.1&quot;, version2 = &quot;1.1&quot; &lt;/br&gt;
//    **Output**: -1 &lt;/br&gt;
//    **Explanation**: version1&#39;s revision 0 is &quot;0&quot;, while version2&#39;s revision 0 is &quot;1&quot;. 0 &lt; 1, so version1 &lt; version2.
//
//    ### Constraints:
//    - `1 &lt;= version1.length, version2.length &lt;= 500`
//    - `version1` and `version2` only contain digits and `&#39;.&#39;`.
//    - `version1` and `version2` are valid version numbers.
//    - All the given revisions in `version1` and `version2` can be stored in a **32-bit integer**.
    fun compareVersions(v1: String, v2: String): Int{
        val ver1 = v1.split(".")
        val ver2 = v2.split(".")

        for(i in 0 until ver1.size.coerceAtLeast(ver2.size)){
            val n1 = ver1.getOrElse(i) {"0"}.toInt()
            val n2 = ver2.getOrElse(i) {"0"}.toInt()
            if(n1 < n2) return -1
            if(n1 > n2) return 1
        }

        return 0
    }
}